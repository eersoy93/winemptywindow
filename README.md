# WinEmptyWindow
A template project written with C++ and Windows API for building new Windows API C++ GUI apps.

# Author
Erdem Ersoy (eersoy93) (with the help of ChatGPT)

# Copyright and License
Copyright (c) 2023 Erdem Ersoy (eersoy93)

Licensed with MIT license. See LICENSE file for full text.

